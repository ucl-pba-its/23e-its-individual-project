![Build Status](https://gitlab.com/ucl-pba-its/23e-its-individual-project/badges/main/pipeline.svg)

# 23e-its-individual-project

Side til undervisningsmateriale vedr. selvvalgt fordybelses projekt 2. semester efterår 2023.

# Efter projektet er klonet

For at se en preview af de ændringer man laver på sit lokale branch, kan der opsættes en lokal server, som giver
mulighed for at se hjemmesiden med de ændringer eller tilføjelser man har lavet lokalt.
For at undgå konflikt med evt. andre 3. parts afhæningheder på sin lokale maskine, bør man opsætte en virtuelt
python først.

1. Lav et virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment) windows: `py -m venv env`
2. Aktiver virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment) windows: `.\env\Scripts\activate`
3. Installer pip dependencies `pip install -r requirements.txt`
4. Kør siden lokalt `mkdocs serve` fra **pages** mappen

## Dokumentation

- MKDocs [https://www.mkdocs.org/](https://www.mkdocs.org/)
- Theme [https://squidfunk.github.io/mkdocs-material/](https://squidfunk.github.io/mkdocs-material/)
- More on Theme [https://squidfunk.github.io/mkdocs-material/](https://squidfunk.github.io/mkdocs-material/)
- Git revision plugin [https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/](https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/)
- linkchecker [https://github.com/manuzhang/mkdocs-htmlproofer-plugin](https://github.com/manuzhang/mkdocs-htmlproofer-plugin)
- PDF builder [https://github.com/brospars/mkdocs-page-pdf](https://github.com/brospars/mkdocs-page-pdf)

## Praktisk information

Link til website: [https://ucl-pba-its.gitlab.io/23e-its-individual-project/](https://ucl-pba-its.gitlab.io/23e-its-individual-project/)
