---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Selvvalgt fordybelsesområde

På dette website finder du ugeplaner, opgaver, dokumenter og links til brug i valgfaget *selvvalgt fordybelsesområde*

Det primære formål med valgfaget *selvvalgt fordybelsesområde* er at give den studerende mulighed for at vælge og arbejde med et fordybelses område indenfor området IT Sikkerhed.  
Det sekundære mål er at den studerende får mulighed for at træne arbejde i et selvstændigt projekt og de udfordringer det kan medføre.  
Den studerende opsætter selv læringsmål (skabelon herunder) ud fra det valgte emne og indenfor rammerne i uddannelsens mål for læringsudbytte fra den nationale studieordning kapitel 1, se: [https://esdhweb.ucl.dk/D22-1980440.pdf](https://esdhweb.ucl.dk/D22-1980440.pdf)  

Fordybelses området kan for eksempel være at dykke ned i hvordan et specifikt stykke sikkerheds software konfigureres og/eller bruges, det kan være sikkerhed indenfor et specifikt område som OT eller IoT eller at dykke ned i et specifikt sikkerheds emne som phishing eller purple teaming.  
Rammerne er brede, men det er vigtigt at den studerende vælger et emne udfra en oprigtig interesse og nysgerrighed.

Den studerende udarbejder i starten af projektet en projektbeskrivelse der indeholder:  

1. Emne
2. Beskrivelse af projektet
3. Læringsmål for projektet (se herunder)
4. Milepæls plan for hele projektet
5. Link til gitlab projekt

Projektbeskrivelsen skal afleveres efter første undervisningsgang og godkendes af underviser 

Igennem projektet skal den studerende deltage i obligatoriske vejledningsmøder med underviser.

Projektplanen sammen med et offentlig tilgængeligt gitlab projekt der beskriver og dokumenterer projektet samt den studerendes reflektion over opfyldelse af læringsmålene, udgør eksamensafleveringen.  

Faget er på 5 ECTS point (135 arbejdstimer) og bedømmes på 7 trins skalaen ud fra de opstillede læringsmål, samt et helhedsindtryk af projektet.

# Skabelon til formulering af læringsmål

Læringsmål ordnes efter viden, færdigheder og kompetencer

**Viden**

Den studerende har viden om...

- ..
- ..


**Færdigheder**

Den studerende kan ...

- ..
- ..

**Kompetencer**

Den studerende kan ...

- ..
- ..

