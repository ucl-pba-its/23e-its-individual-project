# Titel (det valgte fordybelses emne)

# Indhold

Beskrivelse af projektet.

# Læringsmål

Den studerende opsætter selv læringsmål (skabelon herunder) ud fra det valgte emne og indenfor rammerne i uddannelsens mål for læringsudbytte fra den nationale studieordning kapitel 1, se: [https://esdhweb.ucl.dk/D22-1980440.pdf](https://esdhweb.ucl.dk/D22-1980440.pdf)  
Det er ikke alle læringsmål der skal opfyldes, vælg dem der passer til det valgte fordybelsesområde.

Læringsmål skal ordnes efter viden, færdigheder og kompetencer

**Viden**

Den studerende har viden om...

- ..
- ..


**Færdigheder**

Den studerende kan ...

- ..
- ..

**Kompetencer**

Den studerende kan ...

- ..
- ..

# Milepæls plan

Her angives deadlines (datoer) for hvilke milepæle der arbejdes efter i projektet
Der skal angives en milepæl pr. uge fra og med uge 36 til og med uge 46

| Deadline   | Milepæl                       |
| :--------- | :---------------------------- |
| 2023-09-05 | Projekt beskrevet og planlagt |
| 2023-09-12 |                               |
| 2023-09-19 |                               |
| 2023-09-26 |                               |
| 2023-10-03 |                               |
| 2023-10-10 |                               |
| 2023-10-24 |                               |
| 2023-10-31 |                               |
| 2023-11-07 |                               |
| 2023-11-14 | Eksamensaflevering færdig     |

# Link til gitlab projekt 

Her skrives link til det offentligt tilgængelige gitlab projekt.  
Hvis du vælger også at lave gitlab pages angives det link også her.

# Andet

Hvis der er andet du mener er vigtigt at have med i denne plan angives der her eller i punkter du selv tilføjer

Link til denne fil som [Markdown](https://gitlab.com/ucl-pba-its/23e-its-individual-project/-/blob/main/pages/docs/other_docs/projektplan_skabelon.md)