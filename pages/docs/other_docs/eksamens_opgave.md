---
title: '23E ITS Selvvalgt projekt eksamensbeskrivelse'
subtitle: 'eksamensbeskrivelse'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2022-04-04
email: 'nisi@ucl.dk'
left-header: \today
right-header: eksamensbeskrivelse
skip-toc: false
semester: 23E
hide:
  - footer
---

# Dokumentets indhold

Dette dokument indeholder praktiske informationer samt spørgsmål til eksamen i faget: _xxx_.

# Eksamens beskrivelse

Eksamen er en skriftlig aflevering på wiseflow, den studerendes projektplan samt offentlig tilgængeligt gitlab projekt, der beskriver og dokumenterer projektet, samt den studerendes reflektion over opfyldelse af læringsmålene, udgør eksamensafleveringen.  

Tidsplan for eksamen kan findes på wiseflow.  
Eksamen er med intern censur.

Faget er på 5 ECTS point (135 arbejdstimer) og bedømmes efter 7 trins skalaen.

# Eksamens datoer

- Forsøg 1 - 2023-11-14
- Forsøg 2 - 2023-11-28
- Forsøg 3 - 2023-12-20
